<?php

namespace Shop\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\ReferenceField;

class ShopLaptopsTable extends DataManager
{
    public static function getTableName()
    {
        return 'laptopshop_notebooks';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ObjectException
     */
    public static function getMap()
    {
        return array(
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true
                ]
            ),
            new StringField(
                'NAME',
                ['required' => true]
            ),
            new IntegerField(
                'YEAR'
            ),
            new IntegerField(
                'PRICE'
            ),
            new IntegerField(
                'OPTION_ID'
            ),
            new IntegerField(
                'MODEL_ID'
            ),
			new ReferenceField(
                'MODEL',
                'Shop\Orm\ShopModelsTable',
                array('=this.MODEL_ID' => 'ref.ID'),
            ),
			new ReferenceField(
                'OPTION',
                'Shop\Orm\ShopOptionsTable',
                array('=this.OPTION_ID' => 'ref.ID'),
            ),
        );
    }
}
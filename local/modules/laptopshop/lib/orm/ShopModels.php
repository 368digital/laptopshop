<?php

namespace Shop\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\ReferenceField;

class ShopModelsTable extends DataManager
{
    public static function getTableName()
    {
        return 'laptopshop_models';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ObjectException
     */
    public static function getMap()
    {
        return array(
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true
                ]
            ),
            new StringField(
                'NAME',
                ['required' => true]
            ),
            new IntegerField(
                'BRAND_ID'
            ),
            new IntegerField(
                'OPTION_ID'
            ),
            new ReferenceField(
                'BRAND',
                'Shop\Orm\ShopBrandsTable',
                array('=this.BRAND_ID' => 'ref.ID')
            ),
        );
    }
}
<?php

namespace Shop\Orm;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;

class ShopOptionsTable extends DataManager
{
    public static function getTableName()
    {
        return 'laptopshop_options';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ObjectException
     */
    public static function getMap()
    {
        return array(
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true
                ]
            ),
            new StringField(
                'NAME',
                ['required' => true]
            )
        );
    }
}
<?php
Bitrix\Main\Loader::IncludeModule("laptopshop");

$arClasses = [
    'Shop\Orm\ShopBrandsTable'=>'lib/orm/ShopBrands.php',
    'Shop\Orm\ShopModelsTable'=>'lib/orm/ShopModels.php',
    'Shop\Orm\ShopLaptopsTable'=>'lib/orm/ShopLaptops.php',
    'Shop\Orm\ShopOptionsTable'=>'lib/orm/ShopOptions.php',
];

Bitrix\Main\Loader::registerAutoloadClasses(
    'laptopshop',
    $arClasses
);
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
    "custom:laptop.detail",
    "",
    [
        'ELEMENT_NAME' => $arResult['LAPTOP'],
        'SEF_FOLDER' => $arParams['DIRECTORY']
    ]
);
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
    "custom:laptop.list",
    "",
    [
        'METHOD' => 'ShopModelsTable',
        'VARS' => [
            'BRAND' => $arResult['BRAND']
        ],
        'SEF_FOLDER' => $arParams['DIRECTORY'],
    ]
);
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
    "custom:laptop.list",
    "",
    [
        'METHOD' => 'ShopLaptopsTable',
        'VARS' => [
            'BRAND' => $arResult['BRAND'],
            'MODEL' => $arResult['MODEL']
        ],
        'ORDER' => [
            'YEAR' => $_REQUSET['YEAR'],
            'PRICE' => $_REQUSET['PRICE'],
        ],
        'ELEMENT_COUNT' => $_REQUSET['ELEMENTS'],
        'SEF_FOLDER' => $arParams['DIRECTORY'],
        'SHOW_FILTER' => $arResult['SHOW_FILTER']
    ]
);
?>
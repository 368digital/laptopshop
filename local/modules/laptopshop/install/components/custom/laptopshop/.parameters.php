<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "DIRECTORY2" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("DIRECTORY"),
            "TYPE" => "STRING",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
    ),
);

?>

<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SHOP_NAME"),
	"DESCRIPTION" => GetMessage("SHOP_DESCRIPTION"),
	"COMPLEX" => "Y",
	"SORT" => 10,
	"PATH" => array(
		"ID" => "laptopshop",
        "NAME" => GetMessage('SHOP_PATH_NAME')
	)
);
?>
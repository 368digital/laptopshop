<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

$arDefaultUrlTemplates404 = array(
    "brands" => "",
    "detail" => "detail/#LAPTOP#/",
    "models" => "#BRAND#/",
    "laptops" => "#BRAND#/#MODEL#/",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
    "MODEL",
    "LAPTOP",
    "BRAND",
);

$arVariables = array();

$arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, []);
$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, []);

$engine = new CComponentEngine($this);
if (CModule::IncludeModule('iblock'))
{
    $engine->addGreedyPart("#SECTION_CODE_PATH#");
    $engine->setResolveCallback(array(
        "CIBlockFindTools",
        "resolveComponentEngine"
    ));
}
$componentPage = $engine->guessComponentPath($arParams["DIRECTORY"], $arUrlTemplates, $arVariables);

if (!$componentPage)
{
    $componentPage = 'brands';
}


CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

$arResult = array(
    "FOLDER" => $arParams["DIRECTORY"],
    "URL_TEMPLATES" => $arUrlTemplates,
    "VARIABLES" => $arVariables,
    "ALIASES" => $arVariableAliases,
);

Bitrix\Main\Loader::IncludeModule("laptopshop");

$arResult['CHAIN_ITEMS'] = [];

switch ($componentPage)
{
    case 'models':

        $arResult['BRAND'] = $arVariables['BRAND'];
        break;
    case 'laptops':

        $arResult['BRAND'] = $arVariables['BRAND'];
        $arResult['MODEL'] = $arVariables['MODEL'];
        $arResult['SHOW_FILTER'] = 'Y';
        break;
    case 'detail':

        $arResult['LAPTOP'] = $arVariables['LAPTOP'];
        break;
    default:

        $componentPage = "brands";
        break;
}

$this->IncludeComponentTemplate($componentPage);


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

?>

<div class="list-group">
    <?if(isset($arResult['ITEM']['BRAND_NAME'])){?>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1"><?=GetMessage('BRAND')?></title>
            </div>
            <strong class="mb-1"><?=$arResult['ITEM']['BRAND_NAME']?></strong>
        </div>
    <?}?>
    <?if(isset($arResult['ITEM']['MODEL_NAME'])){?>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1"><?=GetMessage('MODEL')?></title>
            </div>
            <strong class="mb-1"><?=$arResult['ITEM']['MODEL_NAME']?></strong>
        </div>
    <?}?>
    <?if(isset($arResult['ITEM']['OPTION_NAME'])){?>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1"><?=GetMessage('OPTION')?></title>
            </div>
            <strong class="mb-1"><?=$arResult['ITEM']['OPTION_NAME']?></strong>
        </div>
    <?}?>
    <?if(isset($arResult['ITEM']['YEAR'])){?>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1"><?=GetMessage('YEAR')?></title>
            </div>
            <strong class="mb-1"><?=$arResult['ITEM']['YEAR']?></strong>
        </div>
    <?}?>
    <?if(isset($arResult['ITEM']['PRICE'])){?>
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1"><?=GetMessage('PRICE')?></title>
            </div>
            <strong class="mb-1"><?=number_format($arResult['ITEM']['PRICE'], 0, '', ' ' )?> ₽</strong>
        </div>
    <?}?>
</div>
<?

?>



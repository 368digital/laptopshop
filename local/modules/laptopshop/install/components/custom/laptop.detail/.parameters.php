<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(

	"PARAMETERS" => array(
		"SEF_FOLDER" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SEF_FOLDER"),
			"TYPE" => "STRING",
		),
		"ELEMENT_NAME" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SHOP_ELEMENT_NAME"),
			"TYPE" => "STRING",
		),

		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);

?>

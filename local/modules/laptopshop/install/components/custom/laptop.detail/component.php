<?
if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
    die();
}

global $APPLICATION;

if($this->startResultCache(false)) {

    $arResult['TITLE'] = $arParams['ELEMENT_NAME'];

    $res = Shop\Orm\ShopLaptopsTable::getList([
        'filter' => [
            'NAME' => $arParams['ELEMENT_NAME']
        ],
        'select' => [
            'NAME',
            'YEAR',
            'PRICE',
            'MODEL_NAME' => 'MODEL.NAME',
            'BRAND_NAME' => 'MODEL.BRAND.NAME',
            'OPTION_NAME' => 'OPTION.NAME'
        ]
    ]);

    if($item = $res->fetch()){
        $arResult['ITEM'] = $item;

        $arResult['CHAIN_ITEMS'][] = [
            'TITLE' => GetMessage('BRANDS_TITLE'),
            'URL'   => $arParams["SEF_FOLDER"]
        ];

        $arResult['CHAIN_ITEMS'][] = [
            'TITLE' => $item['BRAND_NAME'],
            'URL'   => $arParams["SEF_FOLDER"] . $item['BRAND_NAME'] . '/'
        ];

        $arResult['CHAIN_ITEMS'][] = [
            'TITLE' => $item['MODEL_NAME'],
            'URL'   => $arParams["SEF_FOLDER"] . $item['BRAND_NAME'] . '/' . $item['MODEL_NAME'] . '/'
        ];

        $arResult['CHAIN_ITEMS'][] = [
            'TITLE' => $item['NAME'],
            'URL'   => ''
        ];
    }

  $this->IncludeComponentTemplate();
} else {
    $this->abortResultCache();
}

foreach($arResult['CHAIN_ITEMS'] as $item){
    $APPLICATION->AddChainItem($item['TITLE'], $item['URL']);
}

$APPLICATION->SetTitle($arResult['TITLE']);


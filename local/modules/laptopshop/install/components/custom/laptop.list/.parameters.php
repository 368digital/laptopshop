<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(

	"PARAMETERS" => array(
		"METHOD" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SHOP_LIST_METHOD"),
			"TYPE" => "LIST",
			"VALUES" => [
				'ShopBrandsTable' => 'ShopBrandsTable',
				'ShopModelsTable' => 'ShopModelsTable',
				'ShopLaptopsTable' => 'ShopLaptopsTable',
				'ShopOptionsTable' => 'ShopOptionsTable',
			],
		),
		"ELEMENT_COUNT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SHOP_LIST_ELEMENT_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => "3",
		),
		"SEF_FOLDER" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SHOP_LIST_SEF_FOLDER"),
			"TYPE" => "STRING",
		),
		"SHOW_FILTER" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SHOP_LIST_SHOW_FILTER"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);

?>

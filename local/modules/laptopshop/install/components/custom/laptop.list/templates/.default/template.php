<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?>

<?if($arParams['SHOW_FILTER'] == 'Y'){?>
    <form id="listForm" action="">
        <div class="row mb-3">
            <div class="col-sm">
                <select class="form-select" name="YEAR">
                    <option value="0" selected>Год</option>
                    <option value="asc" <?=$_REQUEST['YEAR'] == 'asc' ? 'selected' : ''?>>По возрастанию</option>
                    <option value="desc" <?=$_REQUEST['YEAR'] == 'desc' ? 'selected' : ''?>>По убыванию</option>
                </select>
            </div>
            <div class="col-sm">
                <select class="form-select" name="PRICE">
                    <option value="0"selected>Цена</option>
                    <option value="asc" <?=$_REQUEST['PRICE'] == 'asc' ? 'selected' : ''?>>По возрастанию</option>
                    <option value="desc" <?=$_REQUEST['PRICE'] == 'desc' ? 'selected' : ''?>>По убыванию</option>
                </select>
            </div>
            <div class="col-sm">
                <select class="form-select" name="ELEMENTS">
                    <option value="0"selected>Элементов на стр.</option>
                    <option value="3" <?=$_REQUEST['ELEMENTS'] == '3' ? 'selected' : ''?>>3</option>
                    <option value="6" <?=$_REQUEST['ELEMENTS'] == '6' ? 'selected' : ''?>>6</option>
                </select>
            </div>
        </div>
    </form>
<?}?>
<div class="row">
    <ul class="list-group">

    <?
    foreach($arResult['ITEMS'] as $item){
        ?>
        <li class="list-group-item">
            <a href="<?=$item['URL']?>"><?=$item['NAME']?></a>
        </li>
        <?
    }
    ?>
    </ul>
</div>
<?php
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult['NAV'],
        "SEF_MODE" => "N",
        "CACHE_TYPE" => "N"
    ),
    false
);

?>

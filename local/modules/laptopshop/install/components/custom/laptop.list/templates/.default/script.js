BX.ready(function(){

    let listForm = BX('listForm');

    BX.bindDelegate(
        listForm,
        'input',
        {
            className: 'form-select'
        },
        function(e){
            listForm.submit();
        }
    );
});
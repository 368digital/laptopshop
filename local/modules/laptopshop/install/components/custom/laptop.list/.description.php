<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SHOP_LIST_NAME"),
	"DESCRIPTION" => GetMessage("SHOP_DESCRIPTION"),
	"SORT" => 30,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "laptopshop",
        "NAME" => GetMessage('SHOP_PATH_NAME')
	),
);

?>
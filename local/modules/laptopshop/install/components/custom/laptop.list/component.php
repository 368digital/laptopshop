<?
if ( ! defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) {
	die();
}

global $APPLICATION;
if($this->startResultCache(
    false,
    [
        $arParams['VARS']['BRAND'],
        $arParams['VARS']['MODEL'],
        $_REQUEST['YEAR'],
        $_REQUEST['PRICE'],
        $_REQUEST['ELEMENTS']
    ]
)) {

    $arResult['CHAIN_ITEMS'] = [];

    switch($arParams['METHOD']){
        case 'ShopModelsTable':
            $arResult['BRAND'] = $arParams['VARS']['BRAND'];

            $arResult['TITLE'] = $arParams['VARS']['BRAND'];

            $arResult['CHAIN_ITEMS'][] = [
                'TITLE' => GetMessage('BRANDS_TITLE'),
                'URL'   => $arParams["SEF_FOLDER"]
            ];

            $arResult['CHAIN_ITEMS'][] = [
                'TITLE' => $arResult['TITLE'],
                'URL'   => ''
            ];

            $res = Shop\Orm\ShopModelsTable::getList([
                'filter' => [
                    'BRAND.NAME' => $arParams['VARS']['BRAND']
                ],
            ]);

            $arResult['ITEMS'] = [];
            while($item = $res->fetch()){
                $item['URL'] = $arParams["SEF_FOLDER"] . $arParams['VARS']['BRAND'] . '/' . strtolower($item['NAME']) . '/';
                $arResult['ITEMS'][] = $item;
            }

            break;
        case 'ShopLaptopsTable':

            $arResult['TITLE'] = $arParams['VARS']['MODEL'];

            $arResult['CHAIN_ITEMS'][] = [
                'TITLE' => GetMessage('BRANDS_TITLE'),
                'URL'   => $arParams["SEF_FOLDER"]
            ];

            $arResult['CHAIN_ITEMS'][] = [
                'TITLE' => $arParams['VARS']['BRAND'],
                'URL'   => $arParams["SEF_FOLDER"] . $arParams['VARS']['BRAND'] . '/'
            ];

            $arResult['CHAIN_ITEMS'][] = [
                'TITLE' => $arParams['VARS']['MODEL'],
                'URL'   => ''
            ];

            $order = [];

            if($_REQUEST['YEAR']){
                $order['YEAR'] = $_REQUEST['YEAR'];
            }

            if($_REQUEST['PRICE']){
                $order['PRICE'] = $_REQUEST['PRICE'];
            }

            $arResult['NAV'] = new \Bitrix\Main\UI\PageNavigation ("nav");

            $arResult['NAV']->allowAllRecords(true)
                ->setPageSize(!$_REQUEST['ELEMENTS'] ? 3 : $_REQUEST['ELEMENTS'])
                ->initFromUri();

            $res = Shop\Orm\ShopLaptopsTable::getList([
                'order' => $order,
                'filter' => [
                    'MODEL.NAME' => $arParams['VARS']['MODEL']
                ],
                'count_total' => true,
                'offset' => $arResult['NAV']->getOffset(),
                'limit' => $arResult['NAV']->getLimit(),
            ]);

            $arResult['NAV']->setRecordCount($res->getCount());

            $arResult['ITEMS'] = [];
            while($item = $res->fetch()){
                $item['URL'] = $arParams["SEF_FOLDER"] . 'detail/' . strtolower($item['NAME']) . '/';
                $arResult['ITEMS'][] = $item;
            }
            break;
        case 'ShopBrandsTable':
            $arResult['TITLE'] = GetMessage('BRANDS_TITLE');

            $arResult['CHAIN_ITEMS'][] = [
                'TITLE' => $arResult['TITLE'],
                'URL'   => ''
            ];

            $componentPage = "brands";
            $res = Shop\Orm\ShopBrandsTable::getList([]);

            $arResult['ITEMS'] = [];
            while($item = $res->fetch()){
                $item['URL'] = $arParams["SEF_FOLDER"] . strtolower($item['NAME']) . '/';
                $arResult['ITEMS'][] = $item;
            }
            break;
    }
    $this->setResultCacheKeys(['TITLE', 'CHAIN_ITEMS', 'NAV']);

    $this->IncludeComponentTemplate();
} else {
    $this->abortResultCache();
}

foreach($arResult['CHAIN_ITEMS'] as $item){
    $APPLICATION->AddChainItem($item['TITLE'], $item['URL']);
}


$APPLICATION->SetTitle($arResult['TITLE']);
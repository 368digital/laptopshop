<?

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

if (! check_bitrix_sessid())
{
	return;
}

?>
<form action="<?=$APPLICATION->GetCurPage()?>">

	<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?=LANG?>">
	<input type="hidden" name="id" value="laptopshop">
	<input type="hidden" name="install" value="Y">
	<input type="hidden" name="step" value="2">

	<table cellpadding="3" cellspacing="0" border="0">
		<?

		if (Option::get('laptopshop', 'SHOP_DELETE_TABLES', 'undefined') == 'undefined')
		{
			?>
			<tr>
				<td>
					<input type="checkbox" name="SHOP_DELETE_TABLES" value="Y" id="SHOP_DELETE_TABLES">
				</td>
				<td>
					<p>
						<label for="SHOP_DELETE_TABLES"><?=Loc::getMessage('SHOP_DELETE_TABLES')?></label>
					</p>
				</td>
			</tr>
			<?
		}

		?>
	</table>

	<br>
	<input type="submit" name="" value="<?=Loc::getMessage('MOD_INSTALL_BUTTON')?>">

</form>

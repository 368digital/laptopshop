<?

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Shop\Orm\ShopBrandsTable;
use Shop\Orm\ShopModelsTable;
use Shop\Orm\ShopLaptopsTable;
use Shop\Orm\ShopOptionsTable;

Class LaptopShop extends CModule
{
    var $MODULE_ID = "laptopshop";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function LaptopShop()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = Loc::getMessage('SHOP_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('SHOP_MODULE_DESC');
    }

    function DoInstall()
    {

        global $DOCUMENT_ROOT, $APPLICATION, $step;

        if($step === '2') {
            $this->InstallDB();
            $this->InstallFiles();
            $APPLICATION->IncludeAdminFile(Loc::getMessage('SHOP_INSTALL_TITLE'), $DOCUMENT_ROOT . "/local/modules/laptopshop/install/step2.php");
        } else {
            $APPLICATION->IncludeAdminFile(Loc::getMessage('SHOP_INSTALL_TITLE'), $DOCUMENT_ROOT . "/local/modules/laptopshop/install/step1.php");
        }

        return true;
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION, $SHOP_SAVE_TABLES, $step;

        Loader::IncludeModule("laptopshop");

        if($step === '2') {


            if($SHOP_SAVE_TABLES !== 'Y') {
                $this->UninstallDB();
            }
            UnRegisterModuleDependences("iblock","OnAfterIBlockElementUpdate","laptopshop","CMainLaptopShop","onBeforeElementUpdateHandler");
            ModuleManager::UnRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage('SHOP_UNINSTALL_TITLE'), $DOCUMENT_ROOT . "/local/modules/laptopshop/install/unstep2.php");
        } else {
            $APPLICATION->IncludeAdminFile(Loc::getMessage('SHOP_UNINSTALL_TITLE'), $DOCUMENT_ROOT . "/local/modules/laptopshop/install/unstep1.php");
        }

        return true;
    }

    function InstallDB(){

        global $SHOP_DELETE_TABLES;

        RegisterModuleDependences("iblock","OnAfterIBlockElementUpdate","laptopshop","CMainLaptopShop","onBeforeElementUpdateHandler");
        ModuleManager::RegisterModule($this->MODULE_ID);

        Loader::IncludeModule("laptopshop");

        if($SHOP_DELETE_TABLES === 'Y'){
            $this->UninstallDB();
        }
        $connection = Application::getInstance()->getConnection();

        if(!$connection->isTableExists(ShopBrandsTable::getTableName())) {
            ShopBrandsTable::getEntity()->createDbTable();
        }
        if(!$connection->isTableExists(ShopModelsTable::getTableName())) {
            ShopModelsTable::getEntity()->createDbTable();
        }
        if(!$connection->isTableExists(ShopLaptopsTable::getTableName())) {
            ShopLaptopsTable::getEntity()->createDbTable();
        }
        if(!$connection->isTableExists(ShopOptionsTable::getTableName())) {
            ShopOptionsTable::getEntity()->createDbTable();
        }

        $this->InstallDemoData();
    }

    function UninstallDB() {

        $connection = Application::getInstance()->getConnection();

        if($connection->isTableExists(ShopBrandsTable::getTableName())) {
            $connection->dropTable(ShopBrandsTable::getTableName());
        }
        if($connection->isTableExists(ShopModelsTable::getTableName())) {
            $connection->dropTable(ShopModelsTable::getTableName());
        }
        if($connection->isTableExists(ShopLaptopsTable::getTableName())) {
            $connection->dropTable(ShopLaptopsTable::getTableName());
        }
        if($connection->isTableExists(ShopOptionsTable::getTableName())) {
            $connection->dropTable(ShopOptionsTable::getTableName());
        }
    }

    function InstallDemoData()
    {
        $arBrands = [
            ['NAME' => 'HP'],
            ['NAME' => 'Lenovo'],
            ['NAME' => 'Acer'],
        ];

        $arModels = [
            [
                'NAME' => 'Pavilion',
                'BRAND_ID' => '1',
                'OPTION_ID' => 1
            ],
            [
                'NAME' => 'IdeaPad',
                'BRAND_ID' => '2',
            ],
            [
                'NAME' => 'Aspire',
                'BRAND_ID' => '3',
            ],
            [
                'NAME' => 'Extensa',
                'BRAND_ID' => '3',
            ],
        ];

        $arLaptops = [
            [
                'NAME' => 'x360',
                'YEAR' => '2021',
                'PRICE' => '66990',
                'MODEL_ID' => '1',
            ],
            [
                'NAME' => '15-eh1017ur',
                'YEAR' => '2022',
                'PRICE' => '76990',
                'MODEL_ID' => '1',
            ],
            [
                'NAME' => '15-ARE05',
                'YEAR' => '2020',
                'PRICE' => '46900',
                'MODEL_ID' => '2',
                'OPTION_ID' => 2
            ],
            [
                'NAME' => 'A315',
                'YEAR' => '2021',
                'PRICE' => '54990',
                'MODEL_ID' => '4',
                'OPTION_ID' => 1
            ],
            [
                'NAME' => 'EX215',
                'YEAR' => '2020',
                'PRICE' => '32990',
                'MODEL_ID' => '4',
                'OPTION_ID' => 3
            ],
            [
                'NAME' => 'EX225',
                'YEAR' => '2021',
                'PRICE' => '35990',
                'MODEL_ID' => '4',
                'OPTION_ID' => 3
            ],
            [
                'NAME' => 'EX265',
                'YEAR' => '2018',
                'PRICE' => '12990',
                'MODEL_ID' => '4',
                'OPTION_ID' => 3
            ],
            [
                'NAME' => 'EX515',
                'YEAR' => '2022',
                'PRICE' => '52990',
                'MODEL_ID' => '4',
                'OPTION_ID' => 3
            ],
            [
                'NAME' => 'EX615',
                'YEAR' => '2023',
                'PRICE' => '99990',
                'MODEL_ID' => '4',
                'OPTION_ID' => 3
            ]
        ];

        $arOptions = [
            [
                'NAME' => 'Опция 1',
            ],
            [
                'NAME' => 'Опция 2',
            ],
            [
                'NAME' => 'Опция 3',
            ],
        ];

        foreach($arBrands as $brand){
            ShopBrandsTable::add(
                $brand
            );
        }
        foreach($arModels as $model){
            ShopModelsTable::add(
                $model
            );
        }
        foreach($arLaptops as $laptop){
            ShopLaptopsTable::add(
                $laptop
            );
        }
        foreach($arOptions as $option){
            ShopOptionsTable::add(
                $option
            );
        }
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/laptopshop/install/components", $_SERVER["DOCUMENT_ROOT"]."/local/components", true, true);
    }
}